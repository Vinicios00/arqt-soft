// Nomenclatura de variáveis

const listTitleFollowers = [
  {
    title: 'User',
    followers: 5
  },
  {
    title: 'Friendly',
    followers: 50,
  },
  {
    title: 'Famous',
    followers: 500,
  },
  {
    title: 'Super Star',
    followers: 1000,
  },
]

export default async function searchUserNameGitHub(req, res) {
  const userNameGitHub = String(req.query.username)

  if (!userNameGitHub) {
    return res.status(400).json({
      message: Please provide an username to search on the github API
    })
  }

  const response = await fetch(https://api.github.com/users/${userNameGitHub});

  if (response.status === 404) {
    return res.status(400).json({
      message: User with username "${userNameGitHub}" not found
    })
  }

  const userGitHub = await response.json()

  const orderListFollowers = list.sort((currentFollowersList, nextFollowersList) =>  nextFollowersList.followers - currentFollowersList.followers); 

  const titleCategory = orderList.find(counterForCategory => userGitHub.followers > counterForCategory.followers)

  const result = {
    userNameGitHub,
    categoryTitle: category.title
  }

  return result
}

getData({ query: {
  username: 'josepholiveira'
}}, {})